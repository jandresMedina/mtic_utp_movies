import 'dart:async';
import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:mtic_utp_movies/helpers/debouncer.dart';
import 'package:mtic_utp_movies/models/models.dart';
import 'package:mtic_utp_movies/models/search_movies_response.dart';

///la clase MovieProvider se extiende de la clase ChangeNotifier
class MoviesProvider extends ChangeNotifier {
  final String _baseUrl = 'api.themoviedb.org';
  final String _apiKey = 'c394ac0ad60633b5fc94da2ad9031283'; /// api proporcionada desde TMDB.
  final String _language = 'es-Es';

  List<Movie> onDisplayMovie = [];
  List<Movie> popularMovie = [];
  int _popularPage = 0;

  ///Se genera una lista vacía para almacenar las películas.
  ///Se genera una lista vacía para almacenar las películas populares.

  Map<int, List<Cast>> moviesCast = {};

  final debouncer = Debouncer(duration: Duration(milliseconds: 500));

  final StreamController<List<Movie>> _suggestionsStreamController =
  StreamController.broadcast();

  ///Se generan los valores de la lista de películas.
  Stream<List<Movie>> get suggestionsStream =>
      _suggestionsStreamController.stream;

  MoviesProvider() {
    /// ignore: avoid_print
    print('Movies Provider Inicializado');

    ///El constructor creado llama al método
    getOnDisplayMovie();
    getPopularMovies();
  }

  ///El código se optimiza con el método getOnDisplayMovie como en getPopularMovies
  ///para las solicitudes http.
  Future<String> _getJsonData(String endpoint, [int page = 1]) async {
    var url = Uri.https(_baseUrl, endpoint,
        {'api_key': _apiKey, 'language': _language, 'page': '$page'});

    /// Espera la respuesta de la petición http, luego decodifica el archivo json enviado por la api.
    final response = await http.get(url); /// trae lo solicitado.
    return response.body;
  }

  getOnDisplayMovie() async {
    final jsonData = await _getJsonData('3/movie/now_playing');
    final nowPlaying = NowPlaying.fromJson(jsonData);

    onDisplayMovie = nowPlaying.results;

    ///las peliculas son listadas.
    ///El NotifierListener redibuja los widget que se modificaron.
    notifyListeners();
  }

  getPopularMovies() async {
    _popularPage++;

    final jsonData = await _getJsonData('3/movie/popular', _popularPage);
    final popularResponse = PopularResponse.fromJson(jsonData);

    ///getPopularmovies lo descompone para volver a ser llamado cambiando el page, manteniendo las películas.

    popularMovie = [...popularMovie, ...popularResponse.results];
    notifyListeners();
  }

// metodo para hacer la peticion http y obtener la lista de actores
  Future<List<Cast>> getMovieCast(int movieId) async {
    /// TO DO: Compara el map.
    ///Al método containskey le pasamos el id de la película, para iniciar una sola vez la petición HTTP y ahorrar
    ///así recursos cada vez que mostramos la página detailsScreen.

    if (moviesCast.containsKey(movieId)) return moviesCast[movieId]!;
    // ignore: avoid_print
    print('pidiendo info al servidor');
    final jsonData = await _getJsonData('3/movie/$movieId/credits');
    final creditsResponse = CreditsResponse.fromJson(jsonData);
    moviesCast[movieId] = creditsResponse.cast;
    return creditsResponse.cast;
  }

  ///la instancia SearchResponse creada por una solicitud HTTPS, que genera un map en la carpeta models en su archivo
  ///search_movies_Response.dart); genera una lista de películas después de invocarse la propiedad results de la instancia search.

  Future<List<Movie>> searchMovies(String query) async {
    final url = Uri.https(_baseUrl, '3/search/movie',
        {'api_key': _apiKey, 'language': _language, 'query': query});

    /// Espera la respuesta de la petición http, luego decodifica el archivo json enviado por la api.
    final response = await http.get(url); //devuelve la respuesta
    final searchResponse = SearchResponse.fromJson(response.body);
    return searchResponse.results;
  }

  void getSuggestionByQuery(String searchTerm) {
    debouncer.value = '';
    debouncer.onValue = (value) async {
      print('tenemos valor a buscar: $value');
      final results = await searchMovies(value);
      _suggestionsStreamController.add(
          results); /// add suma el evento del valor solicitado.
    };

    ///los tiempos antirrebote o debouncer del searchTerm una vez cumplidos se emiten en debouncer.onvalue.

    final timer = Timer.periodic(const Duration(milliseconds: 300), (_) {
      debouncer.value = searchTerm;
    });

    ///Resuelta la petición se cancela con timer.cancel
    Future.delayed(Duration(milliseconds: 301)).then((_) => timer.cancel());
  }
}
