import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:mtic_utp_movies/models/models.dart';
import 'package:mtic_utp_movies/widgets/widgets.dart';

class DetailScreen extends StatelessWidget {
  const DetailScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    ///al seleccionar una película,su contenido se almacena en la variable movie.
    final Movie movie = ModalRoute.of(context)!.settings.arguments as Movie;
    return Scaffold(

      ///singleChildScrollView permite utilizar sliver, lo que NO el customScrollView
      ///Cada sliver se comporta diferente cuando se hace un scroll en el padre.

        body: CustomScrollView(
          slivers: [
            _CustomAppBar(movie: movie),

            ///SliverList este widget permite colocar texto e imagen juntos.

            SliverList(
                delegate: SliverChildListDelegate([
                  _PosterAndTitle(movie: movie),
                  _Overview(movie: movie),
                  CastingCards(movieId: movie.id)
                ]))
          ],
        ));
  }
}

class _CustomAppBar extends StatelessWidget {
  const _CustomAppBar({Key? key, required this.movie}) : super(key: key);
  final Movie movie;
  @override
  Widget build(BuildContext context) {
    return SliverAppBar(
      backgroundColor: Colors.indigo,
      expandedHeight: 200,
      floating: false,
      pinned: true,

      ///Una propiedad de sliver es flexibleSpace que muestra el título del sliverAppbar,
      ///a la vez que lo centra y pasa el widget a su child.

      flexibleSpace: FlexibleSpaceBar(
        titlePadding: const EdgeInsets.all(0),
        centerTitle: true,
        title: Container(
            padding: const EdgeInsets.only(bottom: 10, left: 10, right: 10),
            width: double.infinity,
            alignment: Alignment.bottomCenter,
            color: Colors.black45,
            child: Text(
              movie.originalTitle,
              style: const TextStyle(fontSize: 18),
              textAlign: TextAlign.center,
            )),
        background: FadeInImage(
            fit: BoxFit.cover,
            placeholder: const AssetImage('assets/InfinityLoading.gif'),
            image: NetworkImage(movie.fullBackDropPath)),
      ),
    );
  }
}

class _PosterAndTitle extends StatelessWidget {
  const _PosterAndTitle({Key? key, required this.movie}) : super(key: key);
  final Movie movie;
  @override
  Widget build(BuildContext context) {
    final size = MediaQuery.of(context).size;
    final TextTheme textTheme = Theme.of(context).textTheme;
    return Expanded(
      child: Container(
        margin: const EdgeInsets.only(top: 20),
        padding: const EdgeInsets.symmetric(horizontal: 10),
        child: Row(
          children: [
            Hero(
              tag: movie.heroId!,
              child: ClipRRect(
                borderRadius: BorderRadius.circular(20),
                child: FadeInImage(
                    height: 150,
                    width: 110,
                    fit: BoxFit.contain,
                    placeholder: const AssetImage('assets/no-image.png'),
                    image: NetworkImage(movie.fullPosterImg)),
              ),
            ),

            ///21 px es el espacio del row para darle espacio al widget column que muestra el resumen de la película.

            const SizedBox(
              width: 10,
            ),
            ConstrainedBox(
              constraints: BoxConstraints(maxWidth: size.width - 160),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(
                    movie.title,
                    style: textTheme.headline6,
                    overflow: TextOverflow.ellipsis,
                    maxLines: 2,
                  ),
                  Text(
                    movie.originalTitle,
                    style: textTheme.subtitle2,
                    overflow: TextOverflow.ellipsis,
                    maxLines: 2,
                  ),
                  Row(
                    children: [
                      const Icon(
                        Icons.star_border_outlined,
                        size: 21,
                        color: Colors.orangeAccent,
                      ),
                      const SizedBox(
                        width: 10,
                      ),
                      Text(
                        movie.voteAverage.toString(),
                        style: textTheme.caption,
                      )
                    ],
                  )
                ],
              ),
            )
          ],
        ),
      ),
    );
  }
}

class _Overview extends StatelessWidget {
  const _Overview({Key? key, required this.movie}) : super(key: key);
  final Movie movie;
  @override
  Widget build(BuildContext context) {
    return Container(
      padding: const EdgeInsets.symmetric(horizontal: 20, vertical: 10),
      child: Text(
        movie.overview,
        textAlign: TextAlign.justify,
        style: Theme.of(context).textTheme.subtitle1,
      ),
    );
  }
}
