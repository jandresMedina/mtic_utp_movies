import 'package:flutter/material.dart';
import 'package:mtic_utp_movies/providers/movies_providers.dart';
import 'package:mtic_utp_movies/widgets/widgets.dart';
import 'package:provider/provider.dart';
import 'package:mtic_utp_movies/search/search_delegate.dart';

class HomeScreen extends StatelessWidget {
  const HomeScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    ///Se inicia listen en verdadero (True).
    ///MoviesProvider desde home_screen busca y actualiza las movies, iniciando desde la primera instancia al no haberla la crea.

    final moviesProvider = Provider.of<MoviesProvider>(context, listen: true);

    return Scaffold(
      appBar: AppBar(
        elevation: 0,
        title: const Text('Peliculas en Cine UTP'),
        actions: [
          IconButton(
              onPressed: () =>
                  showSearch(context: context, delegate: MovieSearchDelegate()),
              icon: const Icon(Icons.search_outlined))
        ],
      ),
      body: SingleChildScrollView(
        child: Column(children: [
          const SizedBox(
            height: 10,
          ),
          /// cards de principal movies.
          CardSwiper(
            movies: moviesProvider.onDisplayMovie,
          ),
          /// carrusel de movies
          MovieSlider(
            movies: moviesProvider.popularMovie,
            title: 'Populares',
            onNextPage: () => moviesProvider.getPopularMovies(),
          ),
        ]),
      ),
    );
  }
}
