import 'package:flutter/material.dart';
import 'package:mtic_utp_movies/pages/pages.dart';
import 'package:mtic_utp_movies/providers/movies_providers.dart';
import 'package:provider/provider.dart';
import 'package:flutter/material.dart';

void main() => runApp(const AppState());

///Para poder acceder al proveedor de la Api desde cualquier página, se define la clase AppState como global.
class AppState extends StatelessWidget {
  const AppState({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    ///se returna un Multiprovider con un provider para esta aplicación.

    return MultiProvider(
      child: const MyApp(),
      providers: [
        ///El constructor es obligado a inicializarse en Main con un valor false en lazy.

        ChangeNotifierProvider(
            lazy: false, create: (context) => MoviesProvider())
      ],
    );
  }
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'Peliculas',
      initialRoute: 'home',
      routes: {
        'home': (BuildContext context) => const HomeScreen(),
        'detail': (BuildContext context) => const DetailScreen()
      },
      theme: ThemeData.light()
          .copyWith(appBarTheme: const AppBarTheme(color: Colors.indigo)),
    );
  }
}
