///centra las importaciones

export 'package:mtic_utp_movies/models/movie.dart';
export 'package:mtic_utp_movies/models/now_playing_response.dart';
export 'package:mtic_utp_movies/models/popular_response.dart';
export 'package:mtic_utp_movies/models/credits_response.dart';