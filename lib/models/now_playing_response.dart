///La clase NowPlaying se utiliza como map de la peticion HTTPS, junto con la herramienta quicktipe.io
// To parse this JSON data, do
// final nowPlaying = nowPlayingFromMap(jsonString);

import 'dart:convert';

import 'package:mtic_utp_movies/models/models.dart';

class NowPlaying {
  NowPlaying({
    required this.dates,
    required this.page,
    required this.results,
    required this.totalPages,
    required this.totalResults,
  });

  Dates dates;
  int page;
  List<Movie> results;
  int totalPages;
  int totalResults;

  /// str es recibido como respuesta y convertido en map por el método decode.
  factory NowPlaying.fromJson(String str) =>
      NowPlaying.fromMap(json.decode(str));

  /// String toJson() => json.encode(toMap());
  ///el map es recibido y se le asigna un valor en la clase NowPlaying como propiedad de ella.

  factory NowPlaying.fromMap(Map<String, dynamic> json) => NowPlaying(
    dates: Dates.fromMap(json["dates"]),
    page: json["page"],

    ///results actúa como propiedad que adquiere una lista de películas.
    results: List<Movie>.from(json["results"].map((x) => Movie.fromMap(x))),
    totalPages: json["total_pages"],
    totalResults: json["total_results"],
  );


}

class Dates {
  Dates({
    required this.maximum,
    required this.minimum,
  });

  DateTime maximum;
  DateTime minimum;

  factory Dates.fromJson(String str) => Dates.fromMap(json.decode(str));

  ///lo uso para convertir el map a json para el metodo POST
  // String toJson() => json.encode(toMap());

  factory Dates.fromMap(Map<String, dynamic> json) => Dates(
    maximum: DateTime.parse(json["maximum"]),
    minimum: DateTime.parse(json["minimum"]),
  );


}
