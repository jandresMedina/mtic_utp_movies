///Al igual que el archivo pages, utilizo este archivo de widgets para
///centralizar todos los archivos de widgets.
///Se exportará cuando se les llame desde cualquier lugar de aquí.
export 'package:mtic_utp_movies/widgets/card_swiper.dart';
export 'package:mtic_utp_movies/widgets/casting_cards.dart';
export 'package:mtic_utp_movies/widgets/movie_slider.dart';
