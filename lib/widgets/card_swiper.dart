import 'package:card_swiper/card_swiper.dart';
import 'package:flutter/material.dart';
import 'package:mtic_utp_movies/models/models.dart';

class CardSwiper extends StatelessWidget {
  const CardSwiper({Key? key, required this.movies}) : super(key: key);
  final List<Movie> movies;

  @override
  Widget build(BuildContext context) {
    ///el widget MediaQuery informa la dimensión y orientación del cardSwiper en la pantalla, en este caso 50% altura.

    final size = MediaQuery.of(context).size;

    if (movies.isEmpty) {
      return Container(
        width: double.infinity,
        height: size.height * 0.6,
        child: const Center(
          child: CircularProgressIndicator(),
        ),
      );
    }
    return Container(
      margin: const EdgeInsets.only(top: 0),
      width: size.width * 0.8,
      height: size.height * 0.5,
      child: Swiper(
        itemCount: movies.length,
        layout: SwiperLayout.STACK,
        itemWidth: size.width * 0.7,
        itemHeight: size.height * 0.8,
        itemBuilder: (BuildContext context, int index) {
          final movie = movies[index];

          movie.heroId = 'swiper-${movie.id}';

          return GestureDetector(
            onTap: () =>
                Navigator.pushNamed(context, 'detail', arguments: movie),

            ///El widget Hero, permite las transiciones de pantalla entre homescreen a detailsscreen.
            ///hero requiere un child y un objeto tag único por pantalla.
            /// creamos la clase heroId para identificar cada ClipRRect en el homescreen tanto para el cardswiper y movieslider
            /// para identificarlos como objetos únicos; lo mismo para detailscreen en su widget ClipRRect del widget PosterandTitle, para
            /// unir ambas pantallas como una transición.

            child: Hero(
              tag: movie.heroId!,
              child: ClipRRect(
                borderRadius: BorderRadius.circular(20),
                child: FadeInImage(
                    fit: BoxFit.cover,
                    placeholder: const AssetImage('assets/no-image.png'),
                    image: NetworkImage(movie.fullPosterImg)),
              ),
            ),
          );
        },
      ),
    );
  }
}
