import 'package:flutter/material.dart';
import 'package:mtic_utp_movies/models/models.dart';

class MovieSlider extends StatefulWidget {
  const MovieSlider(
      {Key? key, required this.movies, this.title, required this.onNextPage})
      : super(key: key);
  final List<Movie> movies;
  final String? title;
  final Function
  onNextPage; /// se define esta función que se llama cuando llegamos al final del scroll.

  @override
  State<MovieSlider> createState() => _MovieSliderState();
}

class _MovieSliderState extends State<MovieSlider> {
  ///Para usar las barras scroll, tuve que cambiar la clase MovieSlider de Stateless a Stateful.

  final ScrollController scrollController = ScrollController();

  @override
  void initState() {
    super.initState();

    ///el listener es creado para el controller
    scrollController.addListener(() {
      ///Qué hacemos cuando llegamos al final de la lista de películas populares,
      /// hagamos otra solicitud HTTP para traernos la nueva película.
      /// Pero, ¿cómo sabemos cuándo hemos llegado al final de la lista?
      /// Así que usamos el scrollcontroller y usamos maxScrollExtent para acceder al último píxel
      /// y compararlo con el valor del píxel actual.


      if (scrollController.position.pixels >=
          scrollController.position.maxScrollExtent - 200) {
        widget.onNextPage();
        ///TO DO: HACER solicitudes HTTP

      }
    });
  }

  @override
  void dispose() {
    super.dispose();
    scrollController.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      child: Container(
        width: double.infinity,
        height: 270,
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Padding(
              padding: const EdgeInsets.symmetric(horizontal: 20),
              child: Text(
                widget.title!,
                style:
                const TextStyle(fontWeight: FontWeight.bold, fontSize: 18),
              ),
            ),
            Expanded(
              child: ListView.builder(
                ///utilizo el controller ya definido.
                controller: scrollController,
                scrollDirection: Axis.horizontal,
                itemCount: widget.movies.length,
                itemBuilder: (BuildContext context, int index) => _MoviePoster(
                  movie: widget.movies[index],
                  heroid: '${widget.title}-$index-${widget.movies[index].id}',
                ),
              ),
            )
          ],
        ),
      ),
    );
  }
}

class _MoviePoster extends StatelessWidget {
  const _MoviePoster({Key? key, required this.movie, required this.heroid})
      : super(key: key);
  final Movie movie;
  final String heroid;

  @override
  Widget build(BuildContext context) {
    movie.heroId = heroid;
    if (movie == null) {
      return Container(
        width: double.infinity,
        height: 100,
        child: const Center(
          child: CircularProgressIndicator(),
        ),
      );
    }
    return Container(
      margin: const EdgeInsets.symmetric(horizontal: 10, vertical: 10),
      height: 190,
      width: 130,
      child: Column(
        children: [
          ///con el widget GestureDetector y su método onTap se navega a otra página.
          ///PushNamed es un método que pasa el context y el path, arguments como texto y también pasar
          ///otras instancias  de la película seleccionada en la pantalla detailScreen.

          GestureDetector(
            onTap: () => Navigator.pushNamed(context, 'detail',
                arguments: movie), // la clase movie se pasa como argumento
            child: Hero(
              tag: movie.heroId!,
              child: ClipRRect(
                borderRadius: BorderRadius.circular(20),
                child: FadeInImage(
                    width: 130,
                    height: 190,
                    fit: BoxFit.cover,
                    placeholder: const AssetImage('assets/InfinityLoading.gif'),
                    image: NetworkImage(movie.fullPosterImg)),
              ),
            ),
          ),
          Text(
            movie.title,
            overflow: TextOverflow.ellipsis,
            maxLines: 2,
            textAlign: TextAlign.center,
          ),
        ],
      ),
    );
  }
}
