import 'package:flutter/material.dart';
import 'package:mtic_utp_movies/models/models.dart';
import 'package:mtic_utp_movies/providers/movies_providers.dart';
import 'package:provider/provider.dart';

class MovieSearchDelegate extends SearchDelegate {
  ///generamos cuatro métodos de esta clase
  ///al método searchFieldLabel se le asigna un nombre para mostrar en este caso Buscar Película.
  ///para aplicar filtro de búsqueda a la api de TMDB,en search get movies le pasamos nuestra Api_Key
  ///a una consulta de referencia (query) se obtiene el endpoint y con quicktpe convertimos la petición
  ///en un map (clase de dart).

  @override
  String get searchFieldLabel => 'Buscar Pelicula';

  @override
  List<Widget>? buildActions(BuildContext context) {
    ///el metodo buildactions muestra botones de búsqueda.
    return [
      ///con este botón borramos la consulta y la caja queda vacía.
      IconButton(
          onPressed: () {
            query = '';
          },
          icon: const Icon(Icons.clear))
    ];
  }

  @override
  Widget? buildLeading(BuildContext context) {
    /// Este método coloca el botón de previous page,
    /// con el método close de SearchDelegate , cierro el buscador.
    return IconButton(
        onPressed: () {
          close(context, null);
        },
        icon: const Icon(Icons.arrow_back));
  }

  @override
  Widget buildResults(BuildContext context) {
    // TODO: implement buildResults
    return Text('BuildResults');
  }

  @override
  Widget buildSuggestions(BuildContext context) {
    ///este método se inicia con cualquier tecla en el buscador pero solamente el StreamBuilder
    ///muestra algo en pantalla cuando el moviesProvider.suggestionsStream envíe un valor.
    if (query.isEmpty) {
      return Container(
        child: const Center(
          child: Icon(
            Icons.movie_creation_outlined,
            color: Colors.black12,
            size: 150,
          ),
        ),
      );
    }
    print('http request');

    final moviesProvider = Provider.of<MoviesProvider>(context, listen: false);
    moviesProvider.getSuggestionByQuery(query);

    ///se inicia con cualquier tecla estando en el buscador.

    return StreamBuilder(
      stream: moviesProvider.suggestionsStream,
      builder: (_, AsyncSnapshot<List<Movie>> snapshot) {
        if (!snapshot.hasData) {
          return Container(
            child: const Center(
              child: Icon(
                Icons.movie_creation_outlined,
                color: Colors.black12,
                size: 150,
              ),
            ),
          );
        }
        final movies = snapshot.data!;
        return ListView.builder(
            itemCount: movies.length,
            itemBuilder: (_, int index) {
              return _MovieItem(movie: movies[index]);
            });
      },
    );
  }
}

class _MovieItem extends StatelessWidget {
  const _MovieItem({Key? key, required this.movie}) : super(key: key);
  final Movie movie;
  @override
  Widget build(BuildContext context) {
    movie.heroId =
    'search-${movie.id}'; ///cada heroId es único por pantalla.
    return ListTile(
      onTap: () {
        Navigator.pushNamed(context, 'detail', arguments: movie);
      },
      title: Text(movie.title),
      subtitle: Text(movie.originalTitle),
      leading: Hero(
        tag: movie.heroId!,
        child: FadeInImage(
          placeholder: const AssetImage('assets/no-image.png'),
          image: NetworkImage(movie.fullPosterImg),
          width: 70,
          fit: BoxFit.contain,
        ),
      ),
    );
  }
}
