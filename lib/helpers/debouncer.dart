import 'dart:async';

///Debouncer es un antirrebote que escucha las acciones del usuario, espera un tiempo e inicializa el valor de debouncer.
///Debouncer puede ser un string, int, boolean, etc, por su carácter genérico.

class Debouncer<T> {
  Debouncer({required this.duration, this.onValue});

  final Duration duration;

  void Function(T value)? onValue;

  T? _value;
  Timer? _timer;

  ///T es una función de control de dart

  T get value => _value!;

  set value(T val) {
    _value = val;
    _timer?.cancel();

    ///Al recibir una acción del usuario se cancela el timer; de lo contrario se llama el método onValue.
    _timer = Timer(duration, () => onValue!(_value!));


  }
}
